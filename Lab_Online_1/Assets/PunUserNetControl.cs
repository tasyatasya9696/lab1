﻿using Photon.Pun;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
[RequireComponent (typeof (PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks {
    [Tooltip ("The local player instance. Use this to know if the local player isrepresented in the Scene")]
    public static GameObject LocalPlayerInstance;
    private void Awake () {
        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciationwhen levels are synchronized
        if (photonView.IsMine) {
            LocalPlayerInstance = gameObject;
        } else {
            GetComponentInChildren<Camera> ().enabled = false;
            GetComponent<FirstPersonController> ().enabled = false;
        }
    }
}